---
title: "Call Stack"
date: 2022-02-10T12:25:40+05:30
---

**Call Stack** also known as **Stack Frame;** I’m not gonna define what a Call Stack is? that would be very boring like a college text book. Instead I’m gonna tell you Where it’s being used and How does it  work?. And at last I’m gonna define what a call stack is the exact opposite of mainstream education system.

#### Note
I will use Stack Frame/Call Stack interchangebly because it's the same thing so keep that in mind.

### Usage:

A call stack is being used to save a variable of a function in Memory when we call a function.

In other words think about what happen when you call a function like below.

```python
def func(x):
	return x*x

func(10)
Output: 100
```

![Stack](../data/stack.png)
* Remember each function call has it's own Stack Frame/Call Stack whatever and however you wanna call it.
